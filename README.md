# cwinpy-containers

Docker files for CWInPy

These Docker files should be created by running the [`write_cwinpyci_dockerfiles.py`](containers/write_cwinpyci_dockerfiles.py) script and should (generally) not be editted by hand. The script requires the [`luddite`](https://github.com/jumptrading/luddite) package to be installed.
