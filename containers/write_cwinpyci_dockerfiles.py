"""
Script to write CWInPy CI Docker files for required Python versions.
The is copied from the equivalent file used for bilby
https://git.ligo.org/lscsoft/bilby/-/blob/master/containers/write_dockerfiles.py
"""

from datetime import date
from packaging import version
import os
import luddite

with open("dockerfile-cwinpyci-template", "r") as ff:
    template = ff.read()

python_versions = [(3, 9), (3, 10), (3, 11), (3, 12)]
today = date.today().strftime("%Y%m%d")

installfromgit = """
RUN git clone https://git.ligo.org/cwinpy/cwinpy.git && cd cwinpy && pip install . && cd
RUN mkdir -p /cvmfs /hdfs /gpfs /ceph /hadoop
"""

# write Docker files for running the CI and for the development version
for python_major_version, python_minor_version in python_versions:
    with open(
        "dockerfile-cwinpyci-python"
        f"{python_major_version}{python_minor_version}",
        "w"
    ) as ff:
        ff.write(
            "# This dockerfile is written automatically and should not be "
            "modified by hand.\n\n"
        )
        ff.write(template.format(
            date=today,
            python_major_version=python_major_version,
            python_minor_version=python_minor_version
        ))

    # write a container with a development version of CWInPy
    if (python_major_version, python_minor_version) == (3, 9):
        with open(
            "dockerfile-cwinpy-dev-python"
            f"{python_major_version}{python_minor_version}",
            "w"
        ) as ff:
            ff.write(
                "# This dockerfile is written automatically and should not be "
                "modified by hand.\n\n"
            )
            ff.write(template.format(
                date=today,
                python_major_version=python_major_version,
                python_minor_version=python_minor_version
            ))
            ff.write(installfromgit)
            
# write Docker files for release versions
with open("dockerfile-cwinpy-release-template", "r") as ff:
    releasetemplate = ff.read()
    
minrelease = "0.8.0"  # earliest release to create

# get released versions
released_versions = luddite.get_versions_pypi("cwinpy")

for vers in released_versions:
    if version.parse(vers) >= version.parse(minrelease):
        dockerfile = f"dockerfile-cwinpy-release-v{vers}"
        if not os.path.isfile(dockerfile):
            # create release file
            with open(dockerfile, "w") as ff:
                ff.write(
                    "# This dockerfile is written automatically and should not be "
                    "modified by hand.\n\n"
                )
                ff.write(releasetemplate.format(
                    release_version=vers,
                    python_major_version=python_versions[1][0],
                    python_minor_version=python_versions[1][1]
                ))

